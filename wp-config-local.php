<?php
/**
 * Baskonfiguration f�r WordPress.
 *
 * Denna fil inneh�ller f�ljande konfigurationer: Inst�llningar f�r MySQL,
 * Tabellprefix, S�kerhetsnycklar, WordPress-spr�k, och ABSPATH.
 * Mer information p� {@link http://codex.wordpress.org/Editing_wp-config.php 
 * Editing wp-config.php}. MySQL-uppgifter f�r du fr�n ditt webbhotell.
 *
 * Denna fil anv�nds av wp-config.php-genereringsskript under installationen.
 * Du beh�ver inte anv�nda webbplatsen, du kan kopiera denna fil direkt till
 * "wp-config.php" och fylla i v�rdena.
 *
 * @package WordPress
 */

// ** MySQL-inst�llningar - MySQL-uppgifter f�r du fr�n ditt webbhotell ** //
/** Namnet p� databasen du vill anv�nda f�r WordPress */
define('DB_NAME', 'lindensskyltar');

/** MySQL-databasens anv�ndarnamn */
define('DB_USER', 'root');

/** MySQL-databasens l�senord */
define('DB_PASSWORD', 'root');

/** MySQL-server */
define('DB_HOST', 'localhost');

/** Teckenkodning f�r tabellerna i databasen. */
define('DB_CHARSET', 'utf8');

/** Kollationeringstyp f�r databasen. �ndra inte om du �r os�ker. */
define('DB_COLLATE', '');

/**#@+
 * Unika autentiseringsnycklar och salter.
 *
 * �ndra dessa till unika fraser!
 * Du kan generera nycklar med {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Du kan n�r som helst �ndra dessa nycklar f�r att g�ra aktiva cookies obrukbara, vilket tvingar alla anv�ndare att logga in p� nytt.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'MRu%qxmU$t)>$Jnz:)yAR>!3S(l|+h:>^y.()kJ,.{>wF2(%QVU$nxYJe~M`Um*%');
define('SECURE_AUTH_KEY',  'gUCc{|-ns!35If_|gp}w!euAV(vGc-P4ZW4r7pNgNQ[(/eB,e+mhexd:`nDj?zA*');
define('LOGGED_IN_KEY',    'q{w+!Zv-PQi.VZu0IzyC3_&IYCbva:#3<1*^AR>oln}A&2.r;Wi-g@yjCO(fX3p^');
define('NONCE_KEY',        '/3=[mu`VUR+uI[Z`C=?p)|h+j@vY*V+kk,8-VJfBU?N!I?aP3~7m~S(+i+z.Y-Wr');
define('AUTH_SALT',        '<8OW^WUv65n,|mWt{qX(+u)eZJ.#j0{|2)q $D;/oyRNhYpU#mv$e+/0;TX($!+k');
define('SECURE_AUTH_SALT', 'ptc:;b^)e?OT#gk0/9c3`p;3rm=aoNWfsD+X#&TI4;oE/_<4f$c!27-cC8ihkFRA');
define('LOGGED_IN_SALT',   '(eOD*|33j|s{yU4!Zz-hr/g7-jAWmUjf&0//h9V_;9NnlTN%M,]8;Y9H)=n~Jrz9');
define('NONCE_SALT',       'SuOZS~YYTf: bSPh5[|6`|+%j+.Z=R%%>nhQ@B,.-p%kO?/S][R>5TEWSoVy|Br$');

/**#@-*/

/**
 * Tabellprefix f�r WordPress Databasen.
 *
 * Du kan ha flera installationer i samma databas om du ger varje installation ett unikt
 * prefix. Endast siffror, bokst�ver och understreck!
 */
$table_prefix  = 'wp_';

/** 
 * F�r utvecklare: WordPress fels�kningsl�ge. 
 * 
 * �ndra detta till true f�r att aktivera meddelanden under utveckling. 
 * Det �r rekommderat att man som till�ggsskapare och temaskapare anv�nder WP_DEBUG 
 * i sin utvecklingsmilj�. 
 */ 
define('WP_DEBUG', false);

/* Det var allt, sluta redigera h�r! Blogga p�. */

/** Absoluta s�kv�g till WordPress-katalogen. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Anger WordPress-v�rden och inkluderade filer. */
require_once(ABSPATH . 'wp-settings.php');