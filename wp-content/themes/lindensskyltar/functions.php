<?php

// =============================================================================
// FUNCTIONS.PHP
// -----------------------------------------------------------------------------
// Overwrite or add your own custom functions to X in this file.
// =============================================================================

// =============================================================================
// TABLE OF CONTENTS
// -----------------------------------------------------------------------------
//   01. Enqueue Parent Stylesheet
//   02. Additional Functions
// =============================================================================

// Enqueue Parent Stylesheet
// =============================================================================

add_filter( 'x_enqueue_parent_stylesheet', '__return_true' );


// Add child theme styles and scripts
// =============================================================================

function lindens_styles_and_scripts(){
  wp_enqueue_style('lindens', get_stylesheet_directory_uri() . '/library/css/style.css');

  wp_enqueue_script('lindens', get_stylesheet_directory_uri() . '/library/js/lindens-min.js', array('jquery'), '1.0', true);
}

add_action('wp_enqueue_scripts', 'lindens_styles_and_scripts', 9999);



//Register a new sidebar to use on portfolio single pages

function lindens_register_sidebar(){
	register_sidebar( array(
      'name'          => __( 'Portfolio sidebar', '__x__' ),
      'id'            => 'portfolio-sidebar',
      'description'   => __( 'Appears on portfolio.', '__x__' ),
      'before_widget' => '<div id="%1$s" class="widget %2$s">',
      'after_widget'  => '</div>',
      'before_title'  => '<h5 class="h-widget">',
      'after_title'   => '</h5>',
    ) );
}

add_action('widgets_init', 'lindens_register_sidebar');


//Add google analytics

if( WP_DEBUG === false){
  function add_google_analytics(){ ?>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-10899741-32', 'auto');
      ga('send', 'pageview');

    </script>
  <?php }

  add_action('wp_head', 'add_google_analytics');
}

function wpb_list_child_pages() { 

global $post; 

$childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->ID . '&echo=0' );

if ( $childpages ) {

  $string = '<ul>' . $childpages . '</ul>';
}

return $string;

}

add_shortcode('wpb_childpages', 'wpb_list_child_pages');