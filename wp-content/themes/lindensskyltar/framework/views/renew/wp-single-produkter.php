<?php

// =============================================================================
// VIEWS/RENEW/WP-SINGLE-PRODUKTER.PHP
// -----------------------------------------------------------------------------
// Single post output for Produkter.
// =============================================================================

// $fullwidth = get_post_meta( get_the_ID(), '_x_post_layout', true );

?>

<?php get_header(); ?>
  
  <div class="x-container">
    <div class="<?php x_main_content_class(); ?>" role="main">

      <?php while ( have_posts() ) : the_post(); ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <header class="entry-header">
            <h1 class="entry-title">
              <span class="product-name"><?php the_field( 'produktnamn' ); ?></span>
              <div class="product-title"><?php the_title(); ?></div>
            </h1>
          </header>
          <div class="entry-wrap">
            <?php x_get_view( 'global', '_content' ); ?>
            
            <!-- Kontakt, CTA -->
            <div class="x-section contact-section">
              <div class="x-container max width">
                <div class="x-column x-sm x-1-2">
                  <a href="tel:+4660575570"><i class="x-icon x-icon-phone"></i>060-57 55 70</a>
                </div>
                <div class="x-column x-sm x-1-2">
                  <a href="mailto:info@skyltarna.com"><i class="x-icon x-icon-envelope-o"></i>info@skyltarna.com</a>
                </div>
              </div>
            </div>

            <!-- Relaterade poster -->
            <div class="x-section related-products">
              <div class="x-container max width">
                <div class="x-column x-sm x-1-1">
                  <h3><span>Relaterade produkter</span></h3>
                </div>
              </div>
              <div class="x-container max width">
                <div class="x-column x-sm x-1-1">

                  <?php 
                    if( has_term( 'industriskyltar', 'produkttyp' ) ){
                      echo do_shortcode('[the_grid name="Industriskyltar"]');
                    } elseif ( has_term( 'skyltar-inomhus', 'produkttyp' ) ){
                      echo do_shortcode('[the_grid name="Skyltar inomhus"]');
                    } elseif ( has_term( 'skyltar-utomhus', 'produkttyp') ){
                      echo do_shortcode('[the_grid name="Skyltar utomhus"]');
                    } elseif ( has_term( 'lackering-och-specialprodukter', 'produkttyp' ) ){
                      echo do_shortcode('[the_grid name="Specialprodukter"]');
                    } else {
                      echo "Finns inga relaterade produkter";
                    }
                   ?>
                  
                </div>
              </div>
            </div>

          </div>
        </article>
      <?php endwhile; ?>

    </div>
  </div>

<?php get_footer(); ?>